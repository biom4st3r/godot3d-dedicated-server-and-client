extends "res://common/player/player.gd"

func _ready():
	self.is_client = false;

func _physics_process(delta):
	super._physics_process(delta);
	var diff = self.position - self.prev_position;
	diff.y = 0;
	if diff.length_squared() > 1: # bad. Test with latency
		rpc_id(self.sync_id, 's2c_fix_position', self.prev_position, Vector3.ZERO);
		self.position = self.prev_position;
	self.network.broadcast_player_position_update(self.sync_id, self.position, self.rotation);
	move_and_slide();
	prev_position = Vector3(self.position);
