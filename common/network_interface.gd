class_name NetworkInterface extends Node

const addr = '10.0.0.70';
const port = 17771;
const max_players = 100;

func _ready():
	pass
	# OS.get_cmdline_args()

var PLAYERS = {};
var LOADED_MAPS = {}; # Hashset

var CLIENT_PLAYERS = {};

const PLAYER_SCENE = preload("res://common/player/player.tscn");
const CLIENT_PLAYER_SCENE = preload("res://client/player/client_player.tscn");

var client_player: Node;

func _get_player_instance(scene_name: String, is_client: bool, scene: PackedScene=PLAYER_SCENE, post_instance = null) -> Player:
	print('is_server: ', multiplayer.is_server(),' | name: ', scene_name)
	var player = scene.instantiate();
	if post_instance != null:
		(post_instance).call(player);
	player.is_client = is_client;
	player.name = scene_name;
	player.network = self;
	return player;

func _server_player_connect(id):
	print('server: %s connected!' % id)
	# TODO FIXME. This is a godot bug. please submit a reproducable example
	var player = _get_player_instance(str(id), false, PLAYER_SCENE, func(pl): pl.set_script(load("res://server/player/server_player.gd")))
	player.set_multiplayer_authority(id);
	self.PLAYERS[id] = player;
	player.sync_id = id;
	print('server: player: ', player.name)
	await get_tree().create_timer(1.0).timeout
	change_map(id, 'res://common/world/test_world.tscn', 'test_world000')
	for x in PLAYERS.keys():
		if x != id:
			s2c_player_join(id, x);
	player.position.y = 10;
	broadcast_player_join(id);

func _server_player_disconnect(id):
	print('server: %s disconnected!' % id)
	var v = PLAYERS[id];
	PLAYERS.erase(id);
	v.queue_free();
	# TODO
	pass

func start(server: bool):
	var network = ENetMultiplayerPeer.new();
	if server:
		network.create_server(port, max_players);
		print('server: listening on ', str(port))
		network.peer_connected.connect(_server_player_connect);
		network.peer_disconnected.connect(_server_player_disconnect);
		multiplayer.multiplayer_peer = network;
	else:
		network.create_client(addr, port)
		print('client: connecting to %s:%s...' % [addr, str(port)])
		multiplayer.connected_to_server.connect(func(): print("client: connected_to_server"));
		multiplayer.server_disconnected.connect(func(): 
			print("client: server_disconnected")
			get_tree().quit();
		);
		
		multiplayer.multiplayer_peer = network;
		# TODO FIXME. This is a godot bug. please submit a reproducable example
		self.client_player = _get_player_instance(str(multiplayer.get_unique_id()), true, CLIENT_PLAYER_SCENE, func(pl): pl.set_script(load("res://client/player/client_player.gd")));
		self.client_player.set_multiplayer_authority(multiplayer.get_unique_id());
		self.client_player.sync_id = multiplayer.get_unique_id();
		print('client: player: ', self.client_player.name)

# ~~~~~~~~~ RPC/Invoker pairs ~~~~~~~~~
func change_map(id: int, path: String, scene_name: String):
	rpc_id(id, 's2c_change_map', path, scene_name);
	var map;
	if scene_name not in LOADED_MAPS:
		map = (load(path) as PackedScene).instantiate();
		map.name = scene_name;
		self.add_child(map)
		var wall = preload("res://common/world/server_wall.tscn").instantiate();
		map.add_child(wall)
		wall.position = Vector3(10,2,10);
		LOADED_MAPS[scene_name] = map;
	else:
		map = LOADED_MAPS[scene_name];
	map.add_child(PLAYERS[id]);

@rpc(call_remote, authority)
func s2c_change_map(path: String, scene_name: String):
	var f = (load(path) as PackedScene).instantiate();
	f.name = scene_name;
	self.add_child(f);
	client_player.position.y = 10;
	f.add_child(self.client_player);

func broadcast_player_position_update(id: int, position, rotation):
	for x in PLAYERS.keys():
		if x != id:
			rpc_id(x, 's2c_player_position_update', id, position, rotation);

@rpc(authority, unreliable)
func s2c_player_position_update(id: int, position: Vector3, rotation: Vector3):
	if not id in CLIENT_PLAYERS:
		return;
	var player = CLIENT_PLAYERS[id];
	player.new_position = position
	player.new_rotation = rotation;

func broadcast_player_join(joinee_id: int):
	for x in PLAYERS.keys():
		if x != joinee_id:
			rpc_id(x, 's2c_player_joined', joinee_id);

func s2c_player_join(remote: int, joinee_id: int):
	rpc_id(remote, 's2c_player_joined', joinee_id);

@rpc(authority, reliable)
func s2c_player_joined(id: int):
	var other = preload("res://client/player/other_client_player.tscn").instantiate();;
	CLIENT_PLAYERS[id] = other;
	self.client_player.get_parent().add_child(other);

# ~~~~~~~~~~ TEST GUI ~~~~~~~~~~~
func _on_server_pressed():
	for x in self.get_children():
		if x is Button:
			self.remove_child(x);
	start(true);

func _on_client_pressed():
	for x in self.get_children():
		if x is Button:
			self.remove_child(x);
	start(false);
