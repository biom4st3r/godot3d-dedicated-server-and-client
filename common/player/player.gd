class_name Player extends CharacterBody3D

# ~~~ Server Controlled ~~~
var SPEED = 5.0
var JUMP_VELOCITY = 4.5 * 1.5
var RUN_MUL = 2.0
var can_fly = true;
var can_move = true;
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity");

# ~~~ Other ~~~
@export var flying = false;
var is_client = true;
var sync_id: int = -1;
var network: NetworkInterface;

func _ready():
	pass

@rpc(call_remote, any_peer, reliable)
func s2c_fix_position(position: Vector3, velocity: Vector3):
	self.position = position;
	self.velocity = velocity;

func apply_gravity(delta):
	if not is_on_floor() and not flying:
		velocity.y -= gravity * delta * 2 # hax. Change gravity

var prev_position: Vector3;

func _physics_process(delta):
	apply_gravity(delta);
	if is_on_floor() and flying:
		flying = false

