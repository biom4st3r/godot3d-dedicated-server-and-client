extends CharacterBody3D

var SPEED = 5.0
@export var new_position: Vector3;
@export var new_rotation: Vector3;
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity");

func apply_gravity(delta):
	if not is_on_floor():
		velocity.y -= gravity * delta * 2 # hax. Change gravity

func _physics_process(delta):
	self.apply_gravity(delta);
	self.position = self.position.move_toward(new_position, delta * self.SPEED * 2);
	self.rotation = self.rotation.move_toward(new_rotation, delta * 50);
#	if self.updated_position:
#		self.remote_update(delta);

