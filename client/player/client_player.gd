extends "res://common/player/player.gd"

@onready var camera := $Head/Camera3d;

var mouseSensibility = 1200
var mouse_relative_x = 0
var mouse_relative_y = 0

func update_velocity(_delta):
	var input_dir = Input.get_vector("moveLeft", "moveRight", "moveUp", "moveDown");
	var direction = (self.transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction and self.can_move:
		var speed = self.SPEED * (self.RUN_MUL if Input.is_action_pressed("Run") else 1.0)
		self.velocity.x = direction.x * speed
		self.velocity.z = direction.z * speed
	else:
		self.velocity.x = move_toward(self.velocity.x, 0, self.SPEED / 10)
		self.velocity.z = move_toward(self.velocity.z, 0, self.SPEED / 10)
	if Input.is_action_pressed("Jump") and self.can_move:
		if self.flying or self.is_on_floor():
			self.velocity.y += self.JUMP_VELOCITY
	if Input.is_action_pressed("Run") and self.can_move and self.flying:
		self.velocity.y = move_toward(self.velocity.y, self.velocity.y - 1, self.SPEED);

func _physics_process(delta):
	super._physics_process(delta);
	update_velocity(delta);
	move_and_slide();

var last_space = 0

func _input(event):
	if not can_move:
		return;
	if event is InputEventMouseMotion:
		if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
			rotation.y -= event.relative.x / mouseSensibility
			$Head/Camera3d.rotation.x -= event.relative.y / mouseSensibility
			$Head/Camera3d.rotation.x = clamp($Head/Camera3d.rotation.x, deg_to_rad(-90), deg_to_rad(90) )
			mouse_relative_x = clamp(event.relative.x, -50, 50)
			mouse_relative_y = clamp(event.relative.y, -50, 10)
		elif not Input.is_action_pressed("DetachMouse"):
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED;
	if Input.is_action_just_pressed("escape"):
		get_tree().quit()
	if event.is_action_pressed("Jump") and can_fly:
		if Time.get_ticks_msec() - last_space < 300:
			flying = not flying;
		last_space = Time.get_ticks_msec()
	if Input.is_action_just_pressed("DetachMouse"):
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE;
	elif Input.is_action_just_released("DetachMouse"):
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	if Input.is_key_pressed(KEY_P):
		print(self.position)
	if flying and event.is_action_pressed("ui_up"):
		self.position.y += 1;
